import 'package:unpub/unpub.dart' as unpub;

main(List<String> args) async {
  var basedir = '/opt/dart_pkg'; // Base directory to save pacakges
  var db = 'mongodb://mongoadmin:mongoadmin@139.59.175.36:27017/dart_pub?authSource=admin'; // MongoDB uri

  var metaStore = unpub.MongoStore(db);
  await metaStore.db.open();

  var packageStore = unpub.FileStore(basedir);
  var email = 'publisher@hackerpunch.com';

  var app = unpub.App(
    metaStore: metaStore,
    packageStore: packageStore,
    overrideUploaderEmail: email,
  );

  var server = await app.serve('0.0.0.0', 4000);
  print('Serving at http://${server.address.host}:${server.port}');
}